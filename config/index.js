require('dotenv').config();

const config = {
    dev: process.env.NODE_ENV.trim() !== 'production',
    port: process.env.PORT || 3000,
    cors: process.env.CORS,
    
    dbUser: process.env.DB_USER,
    dbPassword: process.env.DB_PASSWORD,
    dbHost: process.env.DB_HOST,
    dbName: process.env.DB_NAME,

    email: process.env.EMAIL,
    emailUsername: process.env.EMAIL_USERNAME,
    emailPassword: process.env.EMAIL_PASSWORD,
    emailPort: process.env.EMAIL_PORT,
    emailService: process.env.EMAIL_SERVICE,

    authAdminUsername: process.env.AUTH_ADMIN_USERNAME,
    authAdminPassword: process.env.AUTH_ADMIN_PASSWORD,
    authAdminEmail: process.env.AUTH_ADMIN_EMAIL,
    authJwtSecret: process.env.AUTH_JWT_SECRET
}

module.exports = { config }; 