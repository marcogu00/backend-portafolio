const express = require('express');
const app = express();
const cors = require('cors');
const cookieParser = require('cookie-parser')


const { config } = require('./config');
const usersApi = require('./routes/users');
const projectsApi = require('./routes/projects');
const authApi = require('./routes/auth');
const otherRoutes = require('./routes/others');

const { logErrors, wrapErrors, errorHandler } = require('./utils/middleware/errorHandlers');
const notFoundHandler = require('./utils/middleware/notFoundHandler');

// Body parser
app.use(express.json());
app.use(cors({
    origin: [
        'http://localhost:3000',
        'https://mgutierrez.cl',
        'https://marcogu00portafolio.herokuapp.com/',
        'http://marcogu00portafolio.herokuapp.com/',
        'https://marcogu00portafolio.herokuapp.com',
        'http://marcogu00portafolio.herokuapp.com'
      ],
      credentials: true
}));
app.use(cookieParser());

app.use('/public', express.static(__dirname + '/public'));

authApi(app);
usersApi(app);
projectsApi(app);
otherRoutes(app);

// 404 not Found Hanlder
app.use(notFoundHandler);

// Middlewares de errores
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

app.listen(config.port, function(){
    console.log(`Listening on http://localhost:${config.port}`);
});