const express = require('express');
const multer  = require('multer');
const path = require('path');
const upload = multer();

const ProjectsService = require('../services/projects');
const validationHandler = require('../utils/middleware/validationHandler');
const Resize = require('../utils/middleware/imgResizer');

const {
    projectIdSchema,
    createProjectSchema,
    updateProjectSchema
} = require('../utils/schemas/projects');
const authHandler = require('../utils/middleware/authHandler');

const projectImageUpload = upload.fields([{ name: 'secondary_img', maxCount: 1 }, { name: 'img', maxCount: 1 }, { name: 'gallery', maxCount: 60 }]);

function projectsApi(app){
    const router = express.Router();

    app.use("/api/projects", router);

    const projectsService = new ProjectsService();
    
    router.get('/', async function(request, response, next){
        
        const query = request.query; 

        try {
            const projects = await projectsService.getProjects({ query });
            
            response.status(200).json({
                data: projects,
                message: 'Listed projects.'
            })
        } catch (error) {
            next(error);
        }
    });
    
    router.get('/:projectId', validationHandler(projectIdSchema, 'params'), async function(request, response, next){
        
        const { projectId } = request.params; 

        try {
            const project = await projectsService.getProjectById( projectId );
            
            response.status(200).json({
                data: project,
                message: 'Listed project.'
            })
        } catch (error) {
            next(error);
        }
    });
    
    router.post('/', authHandler, projectImageUpload ,validationHandler(createProjectSchema), async function(request, response, next){
        
        let project = request.body;
        // Array con orden de las imagenes
        let galleryOrder = JSON.parse(project.gallery_order);
        let sizes = [
            {
                width:1366,
                height: null
            },
            {
                width:992,
                height: null
            },
            {
                width:500,
                height: 580
            }
        ];
        
        try {
            
            const imagePath = '/public/images';
            const fileUpload = new Resize(imagePath);
            project.gallery = [];
            
            const mainImg = await fileUpload.save(request.files.img[0].buffer, sizes);
            project.img = {
                desktop: mainImg['1366'],
                tablet:  mainImg['992'],
                mobile: mainImg['500']
            }
            
            const secondaryImg = await fileUpload.save(request.files.secondary_img[0].buffer, sizes);
            project.secondary_img = {
                desktop: secondaryImg['1366'],
                tablet:  secondaryImg['992'],
                mobile: secondaryImg['500']
            }

            for(const img of request.files.gallery){ 
                
                // Buscamos el orden de la imagen segun el nombre del archivo
                let order = galleryOrder.find(g => g.filename == img.originalname);
                
                sizes[2].height = null;

                let savedImg = await fileUpload.save(img.buffer, sizes);
                project.gallery.push({
                    order: order.order,
                    desktop: savedImg['1366'],
                    tablet:  savedImg['992'],
                    mobile: savedImg['500']
                })
            }

            project.technologies = JSON.parse(project.technologies);
            delete project.gallery_order;
            
            const createdProjectId = await projectsService.createProject({ project: project });
            
            response.status(201).json({
                data: createdProjectId,
                message: 'Created project.'
            })
        } catch (error) {
            next(error);
        }
    });

    router.put('/:projectId', validationHandler(projectIdSchema, 'params'), validationHandler(updateProjectSchema), authHandler, async function(request, response, next){
        
        const { projectId } = request.params; 
        const project = request.body; 

        try {
            const updatedProjectId = await projectsService.updateProject({ projectId, project });
            
            response.status(200).json({
                data: updatedProjectId,
                message: 'Updated project.'
            })
        } catch (error) {
            next(error);
        }
    });

    router.delete('/:projectId', validationHandler(projectIdSchema, 'params'), authHandler, async function(request, response, next){
        
        const { projectId } = request.params;

        try {
            const deletedProjectId = await projectsService.deleteProject({ projectId });
            
            response.status(200).json({
                data: deletedProjectId,
                message: 'Deleted project.'
            })
        } catch (error) {
            next(error);
        }
    });
}


module.exports = projectsApi;