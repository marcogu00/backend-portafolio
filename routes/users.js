const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const UsersService = require('../services/users');

const { userIdSchema, createUserSchema, updateUserSchema, loginUserSchema } = require('../utils/schemas/users');

const validationHandler = require('../utils/middleware/validationHandler');

function usersApi(app){
    const router = express.Router();

    app.use("/api/users", router);

    const usersService = new UsersService();
    
    router.get('/', async function(request, response, next){
        const { tags } = request.query;

        try{
            const users = await usersService.getUsers({ tags });

            response.status(200).json({
                data: users,
                message: 'Listed users'
            });
        }catch(error){
            next(error);
        }
    });

    router.get('/:userId', validationHandler(userIdSchema, 'params'), async function(request, response, next){
        const { userId } = request.params;

        try{
            const user = await usersService.getUser({ _id: userId });

            response.status(200).json({
                data: user,
                message: 'Listed user'
            });
        }catch(error){
            next(error);
        }
    });

    router.post('/', validationHandler(createUserSchema) , async function(request, response, next){
        const { body: user } = request;
        try{

            const emailExist = await usersService.getUser({query:{ email: user.email }});

            if(emailExist){
                return response.status(400).json({
                    message: 'El correo ya se encuentra registrado'
                });
            }
            
            user.type = (!user.type) ? 'client' : user.type;

            const salt = await bcrypt.genSalt(10);
            const hashedPassword = await bcrypt.hash(user.password, salt);
    
            user.password = hashedPassword;
            delete user.repeat_password;
            
            user.created_at = Date.now();

            const createdUserId = await usersService.createUser({ user });

            const token = jwt.sign({_id: createdUserId } , process.env.AUTH_JWT_SECRET);
            response.header('Authorization', token);

            response.status(201).json({
                data: createdUserId,
                token: token,
                message: 'User Created.'
            });
        }catch(error){
            next(error);
        }
    });

    router.put('/:userId', validationHandler(updateUserSchema) ,async function(request, response, next){
        const { userId } = request.params; // request.params.userId
        const user = request.body;
        try{
            const updatedUserId = await usersService.updateUser({ userId, user });

            response.status(200).json({
                data: updatedUserId,
                message: 'User Updated.'
            });
        }catch(error){
            next(error);
        }
    });

    router.delete('/:userId',  validationHandler(userIdSchema, 'params'), async function(request, response, next){
        const { userId } = request.params;
        
        try{
            const deletedUserId = await usersService.deleteUser({ userId });

            response.status(200).json({
                data: deletedUserId,
                message: 'User Deleted.'
            });
        }catch(error){
            next(error);
        }
    });
}

module.exports = usersApi;