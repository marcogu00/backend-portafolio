const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { config }  = require('../config');

const UsersService = require('../services/users');
const { loginUserSchema } = require('../utils/schemas/users');

const validationHandler = require('../utils/middleware/validationHandler');

function authApi(app){
    const router = express.Router();

    app.use("/api/auth", router);

    const usersService = new UsersService();
    
    router.post('/', validationHandler(loginUserSchema, 'body'), async function(request, response, next){

        const { email, password } = request.body;

        try{
            const user = await usersService.getUser({query: { email: email }});
            
            if(!user){
                response.status(401).json({ message: 'Email not found.' });
            }

            const validPass = await bcrypt.compare(password, user.password);
    
            if(!validPass) {
                response.status(401).json( { message: 'Invalid password' });
            }

            const token = jwt.sign({_id: user._id } , process.env.AUTH_JWT_SECRET, { expiresIn: '1h' });

            response.cookie("token", token, {
                expires: new Date(Date.now() + 3600000),
                httpOnly: true,
                secure: !config.dev
            });
            
            response.status(201).json({
                message: 'User logged In'
            });

        }catch(error){
            next(error);
        }
    })
}

module.exports = authApi;