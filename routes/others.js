const express = require('express');
const sgMail = require('@sendgrid/mail');
const { config }  = require('../config');
const validationHandler = require('../utils/middleware/validationHandler');

const { contactFormSchema } = require('../utils/schemas/contact');


function other(app){
    const router = express.Router();

    app.use("/", router);

    
    router.post('/contact', validationHandler(contactFormSchema, 'body'), async function(request, response, next){
        
        let data = request.body;
        
        try {
            
            sgMail.setApiKey(config.emailPassword);

            const msg = {
                to: config.email,
                from: `"${data.name}"  <${data.email}>`,
                subject: 'Formulario de Contacto Portafolio',
                html:  `<h2>Nombre: </h2>
                        <h3>${data.name}</h3>
                        <h2>Email: </h2>
                        <p>${data.email}</p>
                        <h2>Mensaje: </h2>
                        <p>${data.message}</p>`
            };

            sgMail.send(msg);

            response.status(200).json({
                message: 'Formulario enviado con exito.'
            })

        } catch (error) {
            next(error);
        }
    })
}

module.exports = other;