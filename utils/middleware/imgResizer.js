const sharp = require('sharp');
const { v4: uuidv4 } = require('uuid');
const path = require('path');

class Resize {
    constructor(folder) {
        this.folder = folder;
    }

    async save(buffer, sizes = [{width: 1366, height: null}]) {
        const filename = Resize.filename();
        let originalFilePath = this.filepath('original/'+filename);
        let result = {};

        // Original image
        await sharp(buffer)
                .jpeg({
                    quality: 80
                })
                .toFile(originalFilePath);
        
        for(const size of sizes){
            
            let defaultPath = this.filepath(size.width+'/'+filename);
            
            await sharp(buffer)
                .resize({
                    width: size.width,
                    height: size.height,
                    withoutEnlargement: true
                })
                .jpeg({
                    quality: 80
                })
                .toFile(defaultPath);
    
            result[size.width] = `${this.folder}/${size.width}/${filename}.jpeg`
        }

        result.original =  `${this.folder}/original/${filename}.jpeg`;
        return result;
    }
  static filename() {
    return `${uuidv4()}`;
  }
  filepath(filename, format = 'jpeg') {

    return path.resolve(__dirname, `../../${this.folder}/${filename}.${format}`)
  }
}
module.exports = Resize;