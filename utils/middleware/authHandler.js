const jwt = require('jsonwebtoken');

module.exports = function(request, response, next){

    const token = request.cookies.token;

    if(!token) {
        return response.status(401).json({
            message: 'Authorization Token not found.'
        });
    }

    try {
        const verified = jwt.verify(token, process.env.AUTH_JWT_SECRET);
        request.user = verified;
        next();
        
    } catch (error) {
        response.status(401).json({
            message: 'Authorization Token is not valid.'
        })
    }
}