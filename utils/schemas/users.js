const joi = require('@hapi/joi');

const userIdSchema = joi.object().keys({ userId: joi.string().regex(/^[0-9a-fA-F]{24}$/) });
const userNameSchema = joi.string().min(2).max(80);
const userLastNameSchema = joi.string().min(2).max(80);
const userAddressSchema = joi.string().max(250);
const userPhoneSchema = joi.string().min(7);
const userPasswordSchema = joi.string().min(8);
const userRepeatPasswordSchema = joi.ref('password');
const userEmailSchema = joi.string().email();
const userTypeSchema = joi.any().allow('client').allow('admin').default('client');

const createUserSchema = joi.object().keys({
    name: userNameSchema.required(),
    last_name: userLastNameSchema.required(),
    address: userAddressSchema,
    phone: userPhoneSchema,
    password: userPasswordSchema.required(),
    repeat_password: userRepeatPasswordSchema,
    email: userEmailSchema.required(),
    type: userTypeSchema
})

const loginUserSchema = joi.object().keys({
    password: userPasswordSchema.required(),
    email: userEmailSchema.required()
})

const updateUserSchema = joi.object().keys({
    name: userNameSchema,
    last_name: userLastNameSchema,
    address: userAddressSchema,
    phone: userPhoneSchema,
    password: userPasswordSchema,
    email: userEmailSchema
})

module.exports = {
    userIdSchema,
    createUserSchema,
    updateUserSchema,
    loginUserSchema
}