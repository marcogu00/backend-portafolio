const joi = require('@hapi/joi');

const messageSchema = joi.string().min(2).max(400);
const nameSchema = joi.string().min(2).max(80);
const emailSchema = joi.string().email();

const contactFormSchema = joi.object().keys({
    message: messageSchema.required(),
    name: nameSchema.required(),
    email: emailSchema.required()
})


module.exports = {
    contactFormSchema
}