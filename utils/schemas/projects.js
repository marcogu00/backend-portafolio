const joi = require('@hapi/joi');

const projectIdSchema = joi.object().keys({ projectId: joi.string().regex(/^[0-9a-fA-F]{24}$/) });
const projectTitleSchema = joi.string().min(2).max(80);
// const projectSmallDecriptionSchema, projectDesignerSchema, projectYearSchema = joi.string().min(2).max(100);
const projectString = joi.string().min(2).max(100);
const projectOrder = joi.string().min(1).max(60);
const projectDecriptionSchema = joi.string().min(2).max(300);
const projectTechnologiesSchema = joi.string().min(2).max(300);
const projectGalleryOrderSchema = joi.string().min(2);
const projectUrlSchema = joi.string().uri({domain: {}});
const featuredProjectSchema = joi.boolean();
const secondaryImgSchema = joi.any();
// const projectImg = ;
// const projectGallery = ;


const createProjectSchema = joi.object().keys({
    title: projectTitleSchema.required(),
    short_desc: projectString.required(),
    description: projectDecriptionSchema.required(),
    technologies: projectTechnologiesSchema.required(),
    year: projectString.required(),
    designer: projectString.required(),
    designer_portfolio: projectUrlSchema.required(),
    url: projectUrlSchema.required(),
    gallery_order:projectGalleryOrderSchema.required(),
    featured: featuredProjectSchema.required(),
    secondary_img: secondaryImgSchema,
    order: projectOrder.required()
    
})

const updateProjectSchema = joi.object().keys({
    title: projectTitleSchema,
    short_desc: projectString,
    description: projectDecriptionSchema,
    technologies: projectTechnologiesSchema,
    year: projectString,
    designer: projectString,
    designer_portfolio: projectUrlSchema,
    url: projectUrlSchema,
    gallery_order:projectGalleryOrderSchema,
    featured: featuredProjectSchema,
    secondary_img: secondaryImgSchema,
    order: projectOrder
})

module.exports = {
    projectIdSchema,
    createProjectSchema,
    updateProjectSchema
}