const mongoLib = require('../lib/mongo');

class usersService {
    constructor(){
        this.collection = 'users';
        this.mongoDB = new mongoLib();
    }
    
    async getUsers({ query }){
        const users = await this.mongoDB.getAll(this.collection, query);
        return users || [];
    }
    
    async getUser({ query }){
        const users = await this.mongoDB.get(this.collection, query);
        return users || null;
    }

    async createUser({ user }){
         
        const createdUser = await this.mongoDB.create(this.collection, user);
        return createdUser || {};
    }
    
    async updateUser({ userId, user }){
        const updatedUserId = await this.mongoDB.update(this.collection, userId, user);
        return updatedUserId || {};
    }
    
    async deleteUser({ userId }){
        const deletedUserId = await this.mongoDB.delete(this.collection, userId);
        return deletedUserId || {};
    }

}

module.exports = usersService;