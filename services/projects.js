const mongoLib = require('../lib/mongo');

class projectsService {
    constructor(){
        this.collection = 'projects';
        this.mongoDB = new mongoLib();
    }
    
    async getProjects({ query }){
        const projects = await this.mongoDB.getAll(this.collection, query);
        return projects || [];
    }
    
    async getProject({ query }){
        const project = await this.mongoDB.get(this.collection, query);
        return project || {};
    }
    
    async getProjectById(id){
        const project = await this.mongoDB.getById(this.collection, id);
        return project || {};
    }

    async createProject({ project }){

        project.created_at = Date.now();
        const createdProject = await this.mongoDB.create(this.collection, project);
        return createdProject || {};
    }
    
    async updateProject({ projectId, project }){
        const updatedProjectId = await this.mongoDB.update(this.collection, projectId, project);
        return updatedProjectId || {};
    }
    
    async deleteProject({ projectId }){
        const deletedProjectId = await this.mongoDB.delete(this.collection, projectId);
        return deletedProjectId || {};
    }

}

module.exports = projectsService;